//
//  HTTPClientService.swift
//  lastFm
//
//  Created by student on 3/28/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation
import SwiftyJSON

class HTTPClientService {
    
    var httpClient: URLSessionHTTPClient
    
    init() {
        httpClient = URLSessionHTTPClient()
    }
    
    func getArtists(callback: @escaping ([Artist]?, Error?) -> Void) {
        
        let parameters: [String: String]
        var artists = [Artist]()
        
        parameters = ["method": "chart.gettopartists", "limit": "\(21)", "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    artists = try artistResponse.getArtistsReponse()
                    
                    callback(artists, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getTracks(callback: @escaping ([Track]?, Error?) -> Void) {
        
        let parameters: [String: String]
        var tracks = [Track]()
        
        parameters = ["method": "chart.gettoptracks", "limit": "\(20)", "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    tracks = try artistResponse.getTracksResponse()
                    
                    callback(tracks, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getListOfArtists(pageOfArtists: Int, callback: @escaping ([Artist]?, Error?) -> Void) {
        
        let parameters: [String: String]
        var artists = [Artist]()
        
        
        parameters = ["method": "chart.gettopartists", "page": "\(pageOfArtists)", "limit": "\(50)", "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    artists = try artistResponse.getArtistsReponse()
                    print(artists.count)
                    
                    callback(artists, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getListOfSongs(pageOfTracks: Int, callback: @escaping ([Track]?, Error?) -> Void) {
        
        let parameters: [String: String]
        var tracks = [Track]()
        
        parameters = ["method": "chart.gettoptracks", "page": "\(pageOfTracks)", "limit": "\(50)", "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    tracks = try artistResponse.getTracksResponse()
                    print(tracks.count)
                    
                    callback(tracks, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getListOfSongsBySearch(pageOfTracks: Int, searchString: String, callback: @escaping ([Track]?, Error?) -> Void) {
        
        let parameters: [String: String]
        var tracks = [Track]()
        
        parameters = ["method": "track.search", "track": searchString,  "page": "\(pageOfTracks)", "limit": "\(50)", "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    tracks = try artistResponse.getTracksBySearch()
                    print(tracks.count)
                    
                    callback(tracks, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getListOfArtistsBySearch(pageOfArtists: Int, searchString: String, callback: @escaping ([Artist]?, Error?) -> Void) {
        
        let parameters: [String: String]
        var artists = [Artist]()
        
        parameters = ["method": "artist.search", "artist": searchString,  "page": "\(pageOfArtists)", "limit": "\(50)", "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    artists = try artistResponse.getArtistsBySearch()
                    print(artists.count)
                    
                    callback(artists, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getArtistInfo(for currentArtist: Artist, callback: @escaping (ArtistInfo?, Error?) -> Void) {
        
        let parameters: [String: String]
        
        if !currentArtist.mbid.isEmpty {
            parameters = ["method": "artist.getinfo", "mbid": currentArtist.mbid, "format": "json"]
            
        } else {
            parameters = ["method": "artist.getinfo", "artist": currentArtist.name, "format": "json"]
        }
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    let artistInfo = try artistResponse.getArtistInfo(for: currentArtist)
                    callback(artistInfo, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
    func getTopAlbums(for currentArtist: Artist, callback: @escaping (TopAlbums?, Error?) -> Void) {
        
        let parameters: [String: String]
        print(currentArtist.mbid)
        
        if !currentArtist.mbid.isEmpty {
            parameters = ["method": "artist.gettopalbums", "mbid": currentArtist.mbid, "format": "json"]
        } else {
            parameters = ["method": "artist.gettopalbums", "artist": currentArtist.name, "format": "json"]
        }
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                callback(nil, error)
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    let topAlbums = try artistResponse.getTopAlbums(for: currentArtist)
                    callback(topAlbums, nil)
                    
                } catch {
                    callback(nil, error)
                }
            }
        }
    }
    
//    func getAlbumInfo(for currentAlbum: Album, callback: @escaping (Album?, Error?) -> Void) {
//    
//    let parameters: [String: String]
//    
//    if !currentArtist.mbid.isEmpty {
//    parameters = ["method": "artist.getinfo", "mbid": currentArtist.mbid, "format": "json"]
//    
//    } else {
//    parameters = ["method": "artist.getinfo", "artist": currentArtist.name, "format": "json"]
//    }
//    
//    httpClient.get(parameters: parameters) { artistResponse, error in
//    if let error = error {
//    callback(nil, error)
//    } else {
//    do {
//    guard let artistResponse = artistResponse as? HttpResponse else {
//    fatalError("artistResponse error")
//    }
//    
//    let artistInfo = try artistResponse.getArtistInfo(for: currentArtist)
//    callback(artistInfo, nil)
//    
//    } catch {
//    callback(nil, error)
//    }
//    }
//    }
//    }
    
    func getImageFromUrl(stringUrl: String, callback: @escaping (UIImage?) -> Void) {
        
        guard let url = URL(string: stringUrl) else {
            print(stringUrl)
            fatalError("HTTPClientService -> getImageFromUrl: url error")
        }
        
        httpClient.getImage(url: url) { (image, error) in
            if error != nil {
                fatalError("Error: \(error.debugDescription)")
            } else {
                callback(image)
            }
        }
    }
}
