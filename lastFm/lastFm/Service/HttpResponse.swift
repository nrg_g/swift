//
//  ArtistResponse.swift
//  test
//
//  Created by student on 3/21/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation
import SwiftyJSON

class HttpResponse{
    
    let json: JSON
    let data: Data
    
    init(json: JSON, data: Data) {
        self.json = json
        self.data = data
    }
    
    func getArtistsReponse() throws -> [Artist] {
        
        let artists: [Artist]
        
        guard let artist = json["artists"]["artist"].array else {
            fatalError("error")
        }
        
        artists = try artist.map(Artist.init(artist:))
        
        return artists
    }
    
    func getArtistsByTagResponse() throws -> [Artist] {
        let artists: [Artist]
        
        guard let artist = json["topartists"]["artist"].array else {
            fatalError("ArtistResponse -> getArtistsByTagResponse error")
        }
        artists = try artist.map(Artist.init(artist:))
        print("jsonArtists count \(artists.count)")
        return artists
    }
    
    func getArtistsBySearch() throws -> [Artist] {
        
        let artists: [Artist]
        
        guard let artist = json["results"]["artistmatches"]["artist"].array else {
            fatalError("ArtistReponse -> getArtistsBySearch Error")
        }
        artists = try artist.map(Artist.init(artist: ))
        print(artists)
        return artists
    }
    
    func getTracksBySearch() throws -> [Track] {
        
        let tracks: [Track]
        
        guard let track = json["results"]["trackmatches"]["track"].array else {
            fatalError("ArtistReponse -> getTracksBySearch Error")
        }
        
        tracks = try track.map(Track.init(track: ))
        
        return tracks
    }
    
    func getTracksResponse() throws -> [Track] {
        
        let tracks: [Track]
        
        guard let track = json["tracks"]["track"].array else {
            fatalError("ArtistReponse -> getTracksResponse Error")
        }
        
        tracks = try track.map(Track.init(track: ))
        
        return tracks
    }
    
    func getArtistInfo(for currentArtist: Artist) throws -> ArtistInfo {
        let artistInfo = try JSONDecoder().decode(ArtistInfo.self, from: data)
        //let artist = Artist(artist: currentArtist, artistInfo: artistInfo)
        return artistInfo
    }
    
    func getTopAlbums(for currentArtist: Artist) throws -> TopAlbums {
        print(data)
        let topAlbums = try JSONDecoder().decode(TopAlbums.self, from: data)
        return topAlbums
    }
}
   

