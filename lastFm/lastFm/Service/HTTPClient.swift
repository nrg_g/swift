//
//  HTTPClient.swift
//  test
//
//  Created by student on 3/18/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ContentType {
    case json
    case plain
}

protocol HTTPClient {
    init(baseUrl: String, apiKey: String)
    func get(addedUrl: String?, parameters: [String: String]?, contentType: ContentType, callback: @escaping (Any?, Error?) -> Void)
}

class URLSessionHTTPClient: HTTPClient {
    let baseUrl: String
    let apiKey :String
    required init(baseUrl: String, apiKey: String) {
        self.baseUrl = baseUrl
        self.apiKey = apiKey
    }
    
    init(){
        
        guard let baseUrl = Bundle.main.infoDictionary?["MY_BASE_URL"] as? String else {
             fatalError()
        }
        self.baseUrl = baseUrl
        guard let apiKey = Bundle.main.infoDictionary?["MY_API_KEY"] as? String else{
            fatalError()
        }
        self.apiKey = apiKey
    }
    
    func get(addedUrl: String? = nil, parameters: [String: String]? = nil, contentType: ContentType = .json,
             callback: @escaping (Any?, Error?) -> Void) {
        let addedUrl = addedUrl ?? ""
        var parameters = parameters ?? [:]
        let path = "\(baseUrl)\(addedUrl)"
        var items = [URLQueryItem]()
        parameters["api_key"] = apiKey
        
        for(key, value) in parameters {
            items.append(URLQueryItem(name: key, value: value))
        }
        
        var url = URLComponents(string: path)
        url?.queryItems = items
        
        let urlRequest = URLRequest(url: (url?.url)!)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                callback(nil, error)
                return
            }
            
            guard let responseData = data else {
                callback(nil, error)
                return
            }
            
            print(responseData)
            let json = JSON(responseData)
            let artistResponse = HttpResponse(json: json, data: responseData)
            callback(artistResponse, error)
        }
        task.resume()
        
    }
    
    func getImage(url: URL, callback: @escaping (UIImage?, Error?) -> Void) {
        
        let urlRequest = URLRequest(url: url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            
            guard error == nil else {
                callback(nil, error)
                return
            }
            
            guard let responseData = data else {
                callback(nil, error)
                return
            }
            
            let image = UIImage(data: responseData)
            callback(image, error)
        }
        task.resume()
    }
    
    //private func - prepare url and default params
    
}

