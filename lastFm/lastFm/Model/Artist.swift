//
//  User.swift
//  test
//
//  Created by  Татьяна on 3/17/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import SwiftyJSON

struct Artist: Decodable {
    var name: String
    var mbid: String
    var images: [Image]?
    var artistInfo: ArtistInfo?
    var topAlbums: TopAlbums?
    
    init(artist: JSON) throws {
        
        name = artist["name"].stringValue
        mbid = artist["mbid"].stringValue
        self.images = artist["image"].arrayValue.map { Image.init(url: $0["#text"].stringValue, size: $0["size"].stringValue) }

    }
    
    init(artist: Artist, artistInfo: ArtistInfo) {
        self = artist
        self.artistInfo = artistInfo
    }
    
    init(artist: Artist, topAlbums: TopAlbums) {
        self = artist
        self.topAlbums = topAlbums
    }
}
