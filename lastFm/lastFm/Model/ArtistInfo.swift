//
//  ArtistInfo.swift
//  test
//
//  Created by student on 3/21/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ArtistInfo: Decodable {
    
    var bio: String
    var playCount: Int
    var listeners: Int
    var similarArtists: [SimilarArtist]
    var images: [Image]
    
    enum CodingKeys: String, CodingKey {
        case artist
    }
    
    
    enum ArtistCodingKeys: String, CodingKey {
        case bio
        case similar
        case stats
        case image
    }
    
    enum SimilarArtistsCodingKeys: String, CodingKey {
        case similarArtists = "artist"
    }
    
    enum StatsCodingKeys: String, CodingKey {
        case listeners
        case playcount
    }
    
    enum BioCodingKeys: String, CodingKey {
        case content
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
       if container.contains(.artist) {
            
            let artist = try container.nestedContainer(keyedBy: ArtistCodingKeys.self, forKey: .artist)
            self.images = try artist.decode([Image].self, forKey: .image)
            
            let similarArtists = try artist.nestedContainer(keyedBy: SimilarArtistsCodingKeys.self, forKey: .similar)
            self.similarArtists = try similarArtists.decode([SimilarArtist].self, forKey: .similarArtists)
            
            let stats = try artist.nestedContainer(keyedBy: StatsCodingKeys.self, forKey: .stats)
            self.listeners = try Int(stats.decode(String.self, forKey: .listeners)) ?? 0
            self.playCount = try Int(stats.decode(String.self, forKey: .playcount)) ?? 0
            
            let bio = try artist.nestedContainer(keyedBy: BioCodingKeys.self, forKey: .bio)
            self.bio = try bio.decode(String.self, forKey: .content)
        } else {
            self.bio = ""
            self.images = []
            self.listeners = 0
            self.playCount = 0
            self.similarArtists = []
        }
    }
}

struct SimilarArtist: Decodable {
    var artistName: String
    var images: [Image]
    
    enum CodingKeys: String, CodingKey {
        case
        artistName = "name",
        images = "image"
    }
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        self.artistName = try container.decode(String.self, forKey: .name)
//        self.images = try container.decode([Image].self, forKey: .image)
//    }
}


