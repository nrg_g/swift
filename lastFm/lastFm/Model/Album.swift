//
//  Album.swift
//  lastFm
//
//  Created by student on 4/1/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation

struct Album: Decodable {
    
    var name: String
    var playcount: Int
    var images: [Image]
   
    enum CodingKeys: String, CodingKey {
        case name
        case playcount
        case image
        case artist
    }
    
    init(from decoder: Decoder) throws {
        
        let album = try decoder.container(keyedBy: CodingKeys.self)
         
        self.name = try album.decode(String.self, forKey: .name)
        
        self.playcount = try album.decode(Int.self, forKey: .playcount)
        
        self.images = try album.decode([Image].self, forKey: .image)

    }
    
    
    
}

struct TopAlbums: Decodable {
    var topAlbums: [Album]
    
    enum CodingKeys: String, CodingKey {
        case topAlbums = "topalbums"
    }
    
    enum TopAlbumsCodingKeys: String, CodingKey {
        case topAlbums = "album"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let topAlbums = try container.nestedContainer(keyedBy: TopAlbumsCodingKeys.self, forKey: .topAlbums)
        
        self.topAlbums = try topAlbums.decode([Album].self, forKey: .topAlbums)
    }
    
}
