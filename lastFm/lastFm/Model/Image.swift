//
//  Image.swift
//  lastFm
//
//  Created by student on 4/3/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation

struct Image : Decodable {
    var url: String
    var size: String
    
    enum CodingKeys: String, CodingKey {
        case url = "#text"
        case size
    }
}
