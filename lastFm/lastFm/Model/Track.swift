//
//  Track.swift
//  test
//
//  Created by  Татьяна on 3/24/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import Foundation
import SwiftyJSON

//struct TopTrack: Decodable {
//    let track: [Track]
//    let pagination: Pagination
//}
//
//struct Pagination: Decodable {
//    let page: UInt
//    let perPage: UInt
//    let totalPages: UInt
//    let total: UInt
//
//
//
//    init(page: UInt, perGape: UInt) {
//        self.page = page
//        self.perPage = perGape
//        self.totalPages = 0
//        self.total = 0
//    }
//
//    var nextPage: Pagination? {
//        //
//
//    }
//
//    var start: Pagination {
//        Pagination(page: 1, perGape: 50)
//    }
//}

struct Track {
    var name: String
    var smallSizeImageUrl: String
    var bigSizeImageUrl: String
    var images: [Image]?
    var artistName: String
    
//    enum CodingKeys: String, CodingKey {
//
//        case 
//    }
    
    init(track: JSON) throws {
        name = track["name"].stringValue
        smallSizeImageUrl = track["image"][1]["#text"].stringValue
        bigSizeImageUrl = track["image"][3]["#text"].stringValue
        artistName = track["artist"]["name"].stringValue
    }
}
