//
//  ArtistInfoViewController.swift
//  test
//
//  Created by student on 3/21/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit
import WebKit

class ArtistInfoViewController: UIViewController, WKUIDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: Properties
    
    private let httpClientService = HTTPClientService()
    
    var pressedTagName: String?
    
    @IBOutlet weak var similarArtistsCollectionView: UICollectionView!
    @IBOutlet weak var artistPhoto: UIImageView!
    @IBOutlet weak var artistBio: UITextView!
    @IBOutlet weak var topAlbumsCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    var isBigUnfolded: Bool = false
    @IBOutlet weak var moreButton: UIButton!
    let defaultText = "Sorry! There is no text yet!"
    var similarArtistsCellId = "SimilarArtistCollectionViewCell"
    var topAlbumCellId = "TopAlbumCollectionViewCell"
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var selectedArtist: Artist!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        similarArtistsCollectionView.delegate = self
        similarArtistsCollectionView.dataSource = self
        topAlbumsCollectionView.delegate = self
        topAlbumsCollectionView.dataSource = self
        
        spinner.transform.scaledBy(x: 2, y: 2)
        spinner.setNeedsDisplay()
        let nibCellArtist = UINib(nibName: similarArtistsCellId, bundle: nil)
        similarArtistsCollectionView.register(nibCellArtist, forCellWithReuseIdentifier: similarArtistsCellId)
        
        let nibCellAlbum = UINib(nibName: topAlbumCellId, bundle: nil)
        topAlbumsCollectionView.register(nibCellAlbum, forCellWithReuseIdentifier: topAlbumCellId)
        loadArtistInfo()
        loadTopAlbums()
        configureBioTextView()
    }
    
    //MARK: Action
    
    
    @IBAction func onMorePressed(_ sender: Any) {
        if !isBigUnfolded {
            artistBio.translatesAutoresizingMaskIntoConstraints = true
            artistBio.sizeToFit()
            artistBio.heightAnchor.constraint(equalToConstant: 500)
            moreButton.setTitle("Hide...", for: .normal)
            artistBio.isScrollEnabled = true
            artistBio.bounces = false
        } else {
            artistBio.translatesAutoresizingMaskIntoConstraints = false
            moreButton.setTitle("More...", for: .normal)
            artistBio.isScrollEnabled = false
        }
        isBigUnfolded = !isBigUnfolded
    }
    
    //MARK: Methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tagSearchSegue" {
            let destination = segue.destination as? TagSearchViewController
            destination!.tag = pressedTagName!
        }
    }
    
    //MARK: CollectionView Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == similarArtistsCollectionView {
        return selectedArtist.artistInfo?.similarArtists.count ?? 0
        } else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == similarArtistsCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: similarArtistsCellId, for: indexPath) as? SimilarArtistCollectionViewCell else {
                fatalError("error")
            }
            
            var similarArtist = selectedArtist?.artistInfo?.similarArtists[indexPath.row]
            cell.artistName.text = similarArtist?.artistName
            
            loadImage(by: similarArtist?.images[2].url ?? "", in: cell.artistPhoto)
            cell.backgroundColor = UIColor.white
            return cell
            
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topAlbumCellId, for: indexPath) as? TopAlbumCollectionViewCell else {
                fatalError("error")
            }
            
            var topAlbum = selectedArtist?.topAlbums?.topAlbums[indexPath.row]
            cell.albumName.text = topAlbum?.name
            loadImage(by: topAlbum?.images[2].url ?? "", in: cell.albumImage)
            cell.backgroundColor = UIColor.white
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 150, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == similarArtistsCollectionView , let cell = collectionView.cellForItem(at: indexPath) as? SimilarArtistCollectionViewCell {
            selectedArtist.name = cell.artistName.text ?? ""
            selectedArtist.mbid = ""
            loadArtistInfo()
            loadTopAlbums()
            scrollView.scrollToTop()
        }
    }
    
    
    //MARK: Private Methods
    
    
    private func loadImage(by stringUrl: String, in imageView: UIImageView) {
        
        if !stringUrl.isEmpty {
            httpClientService.getImageFromUrl(stringUrl: stringUrl) {
                (image) in
                DispatchQueue.main.async {
                    imageView.image = image
                }
            }
        }
    }
    
    private func configureBioTextView() {
        artistBio.frame.size = CGSize.zero
        artistBio.sizeToFit()
        artistBio.isScrollEnabled = false
    }
    
    private func adjustBio() {
        if !artistBio.text.isEmpty {
        artistBio.text = self.artistBio.text.removeFirstCharactersThatAreNotLetters
        let regex = NSRegularExpression("<a.*a>. ")
        artistBio.text = regex.matches(self.artistBio.text)
        } else {
            artistBio.text = "There is no inforamtion yet!"
        }
    }
    
  
    
    
    private func loadArtistInfo(){
        spinner.startAnimating()
        
        httpClientService.getArtistInfo(for: selectedArtist) { (artistInfo, error) in
            if error != nil {
                print("ArtistInfoViewControler -> loadArtistInfo -> Error: \(error.debugDescription)")
            } else {
                self.selectedArtist.artistInfo = artistInfo
            }
            DispatchQueue.main.async {
                self.similarArtistsCollectionView.reloadData()
                self.artistBio.text = self.selectedArtist.artistInfo?.bio
                self.adjustBio()
                self.loadImage(by: self.selectedArtist.artistInfo?.images[4].url ?? "", in: self.artistPhoto)
                self.spinner.stopAnimating()
            }
        }
        
    }
    
    private func loadTopAlbums() {
        httpClientService.getTopAlbums(for: selectedArtist) { (topAlbums, error) in
            if error != nil {
                print("ArtistInfoViewControler -> loadTopAlbums -> Error: \(error.debugDescription)")
            } else {
                self.selectedArtist.topAlbums = topAlbums
            }
            DispatchQueue.main.async {
                self.topAlbumsCollectionView.reloadData()
            }
        }
    }
}

extension String {
    var removeFirstCharactersThatAreNotLetters: String {
        var flag = true
        var string = self
        while(flag){
            if string.first == "\n" {
                string.remove(at: string.index(of: string.first!)!)
            } else {
                flag = false
            }
        }
        return string
    }
}

extension NSRegularExpression {
    
    convenience init(_ pattern: String) {
        do {
            try self.init(pattern: pattern)
        } catch {
            preconditionFailure("Illegal regex: \(pattern)")
        }
    }
    
    func matches(_ string: String) -> String {
        let range = NSRange(location: 0, length: string.utf16.count)
        return stringByReplacingMatches(in: string, options: [], range: range, withTemplate: "")
    }
}


extension UIScrollView {
    func scrollToTop() {
        let offset = CGPoint(x: -self.adjustedContentInset.left, y: -self.adjustedContentInset.top)
        self.setContentOffset(offset, animated: true)
    }
}
