//
//  TopSongsCollectionViewController.swift
//  lastFm
//
//  Created by student on 3/26/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit



class TopSongsCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    //MARK: Properties
    
    @IBOutlet weak var titleBar: UINavigationItem!
    private let httpClientService = HTTPClientService()
    private let topSongsCollectionViewId = "SongCollectionViewCell"
    var tracks: [Track]?
    var indexOfTheCurrentRow = 0
    var currentTrack: Track?
    var isLoading = false
    var pageOfTracks = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibCell = UINib(nibName: topSongsCollectionViewId, bundle: nil)
        collectionView.delegate = self
        collectionView.dataSource = self
        loadTracks()
        self.collectionView!.register(nibCell, forCellWithReuseIdentifier: topSongsCollectionViewId)
    }
    
    // MARK: UICollectionViewDataSource
    
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return tracks?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topSongsCollectionViewId, for: indexPath) as? SongCollectionViewCell else {
            fatalError("TopSongCollectionView -> collectionView -> cell is nil")
        }
        
        let track = tracks![indexPath.row]
        cell.songNameLabel.text = track.name
        
        if track.smallSizeImageUrl == "" {
            cell.songImage.image = UIImage(named: "noImagePng")
        } else {
            if let url = URL(string: track.smallSizeImageUrl) {
                cell.songImage.sd_setImage(with: url)
            }
        }
        
        cell.numberInChartLabel.text = "\(indexPath.row + 1)"
        cell.artistNameLabel.text = track.artistName
        cell.backgroundColor = .white
        //cell.widthAnchor.constraint(equalToConstant: 500)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: UIScreen.main.bounds.width - 5, height: 70)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row > Int(UIScreen.main.bounds.height / cell.bounds.height) {
            titleBar.titleView?.backgroundColor = .black
        }
        if indexPath.row == 1 {
            titleBar.title = "Top Songs"
        }
        let lastElement = tracks!.count - 5
        if (indexPath.row == lastElement) && !isLoading {
            loadTracks()
            isLoading = true
        }
    }
    
    //MARK: Private Functions
    
    private func loadTracks() {
        
        httpClientService.getListOfSongs(pageOfTracks: pageOfTracks) { (tracks, error) in
            if error != nil {
                print("TopSongsCollectionView -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                if tracks?.count ?? 0 > 50 {
                    self.tracks? += tracks!.suffix(from: tracks!.count - 50)
                }
                if self.tracks != nil {
                    self.tracks! += tracks ?? []
                } else {
                    self.tracks = []
                    self.tracks! += tracks ?? []
                }
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.pageOfTracks += 1
                self.isLoading = false
            }
        }
    }
    
    
}
