//
//  ArtistCollectionViewCell.swift
//  lastFm
//
//  Created by student on 3/26/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class ArtistCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var numberInChartLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
