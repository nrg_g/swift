//
//  TopAlbumCollectionViewCell.swift
//  lastFm
//
//  Created by student on 4/1/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class TopAlbumCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var albumName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        albumName.frame.size = CGSize.zero
        albumName.sizeToFit()
        albumName.numberOfLines = 2
        // Initialization code
    }

}
