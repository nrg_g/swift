//
//  SimilarArtistCollectionViewCell.swift
//  lastFm
//
//  Created by  Татьяна on 3/31/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class SimilarArtistCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var artistPhoto: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        artistName.frame.size = CGSize.zero
        artistName.sizeToFit()
        artistName.numberOfLines = 2
        artistPhoto.roundedImage()
        // Initialization code
    }

}

extension UIImageView {
    func roundedImage() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}
