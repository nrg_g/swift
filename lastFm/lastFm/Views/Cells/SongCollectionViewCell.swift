//
//  SongCollectionViewCell.swift
//  lastFm
//
//  Created by student on 3/26/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class SongCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var songImage: UIImageView!
    @IBOutlet weak var numberInChartLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
