//
//  ArtistSearchViewController.swift
//  test
//
//  Created by  Татьяна on 3/24/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class ArtistTracksSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //MARK: Properties
   
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var getArtistsButton: UIButton!
    @IBOutlet weak var getTracksButton: UIButton!
    @IBOutlet weak var artistsResultOfSearchTable: UITableView!
    @IBOutlet weak var tracksResultOfSearchTable: UITableView!
    
    var artists: [Artist]?
    var tracks: [Track]?
    let httpClient = URLSessionHTTPClient()
    let httpClientService = HTTPClientService()
    var isLoading = false
    var pageOfArtists = 1
    var pageOfTracks = 1
    var searchString: String?
    var isSearchButtonClicked = false
    var currentArtist: Artist!
    var buttonColor: UIColor?
    var currentButton: UIButton?
    var buttonsAndTables = [UIButton: UITableView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistsResultOfSearchTable.delegate = self
        artistsResultOfSearchTable.dataSource = self
        tracksResultOfSearchTable.delegate = self
        tracksResultOfSearchTable.dataSource = self
        searchBar.delegate = self
        
        buttonColor = getTracksButton.tintColor
        changeButtonStyle(forPressedButton: getArtistsButton, forSecondButton: getTracksButton)
        
        currentButton = getArtistsButton
        
        buttonsAndTables = [getArtistsButton: artistsResultOfSearchTable, getTracksButton: tracksResultOfSearchTable]
    }
    
    //MARK: Action
    
    @IBAction func onArtistsButtonPressed(_ sender: UIButton) {
        changeTables(currentButton!, sender)
    }
    
    @IBAction func onTracksButtonPressed(_ sender: UIButton) {
        changeTables(currentButton!, sender)
    }
    
    //MARK: TableView methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !artistsResultOfSearchTable.isHidden {
            return artists?.count ?? 0
        } else {
            return tracks?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let artistCellIdentifier = "ArtistTableViewCell"
        let trackCellIdentifier = "TrackTableViewCell"
        
        if tableView == artistsResultOfSearchTable {
            guard let cell = artistsResultOfSearchTable.dequeueReusableCell(withIdentifier: artistCellIdentifier, for: indexPath) as? ArtistsTableViewCell else {
                fatalError("ArtistTableViewCell error")
            }
            print(indexPath.row)
            let artist = artists![indexPath.row]
            cell.artistName.text = artist.name
            
            if artist.images?[2].url == "" {
                cell.artistPhoto.image = UIImage(named: "noImagePng")
            } else {
                if let url = URL(string: artist.images?[2].url ?? "") {
                    cell.artistPhoto.sd_setImage(with: url)
                }
            }
            return cell
            
        } else {
            
            guard let cell = tracksResultOfSearchTable.dequeueReusableCell(withIdentifier: trackCellIdentifier, for: indexPath) as? TrackTableViewCell else {
                fatalError("TrackTableViewCell error")
            }
            
            if let track = tracks?[indexPath.row] {
                cell.trackName.text = track.name
            }
            let track = tracks?[indexPath.row]
            if track?.smallSizeImageUrl == "" {
                cell.trackImage.image = UIImage(named: "noImagePng")
            } else {
                if let url = URL(string: (track?.smallSizeImageUrl)!) {
                    cell.trackImage.sd_setImage(with: url)
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectCell(in: tableView, indexPath)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == artistsResultOfSearchTable {
            let lastElement = artists!.count - 1
            if (indexPath.row == lastElement) && !isLoading {
                loadArtists()
                isLoading = true
            }
        } else {
            let lastElement = tracks!.count - 1
            print(isLoading)
            if (indexPath.row == lastElement) && !isLoading {
                loadTracks()
                isLoading = true
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ArtistInfoViewController {
            let artistInfo = segue.destination as? ArtistInfoViewController
            artistInfo?.selectedArtist = currentArtist
        }
    }
    
    //MARK: SearchBar Functions
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let isSearchBarEmpty = searchBar.text?.isEmpty else {
            print("searchBar")
            return
        }
        if !isSearchBarEmpty {
            isSearchButtonClicked = true
            searchString = searchBar.text ?? ""
            pageOfArtists = 1
            pageOfTracks = 1
            searchBar.resignFirstResponder()
            artists = []
            loadArtists()
            tracks = []
            loadTracks()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        artists = []
        tracks = []
        print("text has been changed")
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    //MARK: Private Functions
    
    private func changeTables(_ currentButton: UIButton, _ pressedButton: UIButton){
        guard let isSearchBarEmpty = searchBar.text?.isEmpty else {
            print("searchBar")
            return
        }
        if !isSearchBarEmpty {
            if currentButton == pressedButton {
                scrollToTop(in: [buttonsAndTables[currentButton]!, buttonsAndTables[pressedButton]!])
            } else {
                buttonsAndTables[currentButton]?.isHidden = true
                buttonsAndTables[pressedButton]?.isHidden = false
                buttonsAndTables[pressedButton]?.reloadData()
                self.currentButton = pressedButton
                changeButtonStyle(forPressedButton: pressedButton, forSecondButton: currentButton)
                scrollToTop(in: [buttonsAndTables[currentButton]!, buttonsAndTables[pressedButton]!])
            }
        }
    }
    
    private func selectCell(in tableView: UITableView, _ indexPath: IndexPath){
        if tableView == artistsResultOfSearchTable {
            let selectedArtist = artists![indexPath.row]
            currentArtist = selectedArtist
            let selectedCell = tableView.cellForRow(at: indexPath)
            selectedCell?.contentView.backgroundColor = tableView.backgroundColor
            performSegue(withIdentifier: "showArtistInfoAfterSearch", sender: self)
        } else {
            print("tracks")
        }
    }
    
    private func changeButtonStyle(forPressedButton pressedButton: UIButton, forSecondButton secondButton: UIButton){
        if pressedButton.tintColor != UIColor.red {
            pressedButton.tintColor = UIColor.red
            secondButton.tintColor = buttonColor
        }
    }
    
    private func scrollToTop(in tableViews: [UITableView]) {
        
        for tableView in tableViews {
            let indexPath = NSIndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
        }
    }
    
    
    private func loadTracks() {
        let searchString = self.searchString ?? ""
        httpClientService.getListOfSongsBySearch(pageOfTracks: pageOfTracks, searchString: searchString) { (tracks, error) in
            
            if error != nil {
                print("ArtistsTracksSearchViewController -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                if tracks?.count ?? 0 > 50 {
                    self.tracks? += tracks!.suffix(from: tracks!.count - 50)
                }
                if self.tracks != nil {
                    self.tracks! += tracks ?? []
                } else {
                    self.tracks = []
                    self.tracks! += tracks ?? []
                }
            }
            DispatchQueue.main.async {
                self.tracksResultOfSearchTable.reloadData()
                self.pageOfTracks += 1
                self.isLoading = false
                if self.isSearchButtonClicked {
                    self.isSearchButtonClicked = false
                }
            }
        }
    }

    private func loadArtists() {
        let searchString = self.searchString ?? ""
        httpClientService.getListOfArtistsBySearch(pageOfArtists: pageOfArtists, searchString: searchString) { (artists, error) in
            
            if error != nil {
                print("ArtistsTracksSearchViewController -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                if artists?.count ?? 0 > 50 {
                    self.artists? += artists!.suffix(from: artists!.count - 50)
                }
                if self.artists != nil {
                    self.artists! += artists ?? []
                } else {
                    self.artists = []
                    self.artists! += artists ?? []
                }
            }
            DispatchQueue.main.async {
                self.artistsResultOfSearchTable.reloadData()
                self.pageOfArtists += 1
                self.isLoading = false
                if self.isSearchButtonClicked {
                    self.isSearchButtonClicked = false
                }
            }
        }
    }
}
