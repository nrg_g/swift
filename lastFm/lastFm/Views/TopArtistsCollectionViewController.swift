//
//  TopArtistsCollectionViewController.swift
//  lastFm
//
//  Created by student on 3/27/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class TopArtistsCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //MARK: Properties
    
    @IBOutlet weak var titleBar: UINavigationItem!
    private let httpClientService = HTTPClientService()
    private let topArtistsCollectionViewId = "ArtistCollectionViewCell"
    var artists: [Artist]?
    var indexOfTheCurrentRow = 0
    var currentArtist: Artist?
    var isLoading = false
    var pageOfArtists = 1
    var selectedArtist: Artist?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let nibCell = UINib(nibName: topArtistsCollectionViewId, bundle: nil)
        collectionView.delegate = self
        collectionView.dataSource = self
        loadArtists()
        self.collectionView!.register(nibCell, forCellWithReuseIdentifier: topArtistsCollectionViewId)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return artists?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topArtistsCollectionViewId, for: indexPath) as? ArtistCollectionViewCell else {
            fatalError("TopArtistCollectionView -> collectionView -> cell is nil")
        }
        
        let artist = artists![indexPath.row]
        cell.artistNameLabel.text = artist.name
        
        if artist.images?[2].url == "" {
            cell.artistImage.image = UIImage(named: "noImagePng")
        } else {
            if let url = URL(string: artist.images?[2].url ?? "") {
                cell.artistImage.sd_setImage(with: url)
            }
        }
        
        cell.numberInChartLabel.text = "\(indexPath.row + 1)"
        cell.backgroundColor = .white
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: UIScreen.main.bounds.width - 5, height: 90)
    }
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row > Int(UIScreen.main.bounds.height / cell.bounds.height) {
        }
        if indexPath.row == 1 {
            titleBar.title = "Top Artists"
        }
        let lastElement = artists!.count - 5
        if (indexPath.row == lastElement) && !isLoading {
            loadArtists()
            isLoading = true
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectCell(in: collectionView, indexPath)
    }
    
    //MARK: NAvigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ArtistInfoViewController {
            let artistInfo = segue.destination as? ArtistInfoViewController
            artistInfo?.selectedArtist = selectedArtist
        }
    }
    
    //MARK: Private functions
    
    private func selectCell(in collectionView: UICollectionView, _ indexPath: IndexPath){
            let selectedArtist = artists![indexPath.row]
            self.selectedArtist = selectedArtist
            let selectedCell = collectionView.cellForItem(at: indexPath)
            selectedCell?.contentView.backgroundColor = collectionView.backgroundColor
            performSegue(withIdentifier: "artistInfo", sender: self)
    }
    
    
    private func loadArtists() {
        httpClientService.getListOfArtists(pageOfArtists: pageOfArtists) { (artists, error) in
            if error != nil {
                print("TopArtistsCollectionView -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                if artists?.count ?? 0 > 50 {
                    self.artists? += artists!.suffix(from: artists!.count - 50)
                } else {
                    if self.artists != nil {
                        self.artists! += artists ?? []
                    } else {
                        self.artists = []
                        self.artists! += artists ?? []
                    }
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.pageOfArtists += 1
                    self.isLoading = false
                }
            }
            
        }
        
        
    }
}


