//
//  TopChartsViewController.swift
//  lastFm
//
//  Created by student on 3/26/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class ChartsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    //MARK: Properties
    
    @IBOutlet weak var topChartsLabel: UILabel!
    @IBOutlet weak var songsLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var topSongsCollectionView: UICollectionView!
    @IBOutlet weak var topArtistsCollectionView: UICollectionView!
    
    var tracks: [Track]?
    var artists: [Artist]?
    var selectedArtist: Artist?
    var httpClient = URLSessionHTTPClient()
    var httpClientService = HTTPClientService()
    let topSongsCollectionViewCellId = "SongCollectionViewCell"
    let topArtistsCollectionViewCellId = "ArtistCollectionViewCell"
    
    override func viewDidLoad() {
        topSongsCollectionView.delegate = self
        topSongsCollectionView.dataSource = self
        topArtistsCollectionView.dataSource = self
        topArtistsCollectionView.delegate = self
        
        let nibCellSong = UINib(nibName: topSongsCollectionViewCellId, bundle: nil)
        let nibCellArtist = UINib(nibName: topArtistsCollectionViewCellId, bundle: nil)
        topArtistsCollectionView.register(nibCellArtist, forCellWithReuseIdentifier: topArtistsCollectionViewCellId)
        topSongsCollectionView.register(nibCellSong, forCellWithReuseIdentifier: topSongsCollectionViewCellId)
        super.viewDidLoad()
        loadTracks()
        loadArtists()
    }
    
    //MARK: TableView Functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == self.topSongsCollectionView {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topSongsCollectionViewCellId, for: indexPath) as? SongCollectionViewCell else {
                fatalError("error")
            }
            let track = tracks?[indexPath.row]
            cell.songNameLabel.text = track?.name
            
            if track?.smallSizeImageUrl == "" {
                cell.songImage.image = UIImage(named: "noImagePng")
            } else {
                if let url = URL(string: (track?.smallSizeImageUrl) ?? "nil") {
                    cell.songImage.sd_setImage(with: url)
                }
            }
            cell.numberInChartLabel.text = String(indexPath.row + 1)
            cell.artistNameLabel.text = track?.artistName
            cell.backgroundColor = UIColor.white
            return cell
            
            
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: topArtistsCollectionViewCellId, for: indexPath) as? ArtistCollectionViewCell else {
                fatalError("error")
            }
            let artist = artists?[indexPath.row] ?? nil
            cell.artistNameLabel.text = artist?.name
            
            if artist?.images?[2].url == "" {
                cell.artistImage.image = UIImage(named: "noImagePng")
            } else {
                if let url = URL(string: (artist?.images?[2].url) ?? "nil") {
                    cell.artistImage.sd_setImage(with: url)
                }
            }
            cell.numberInChartLabel.text = String(indexPath.row + 1)
            cell.backgroundColor = UIColor.white
            return cell
        }
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = 5
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(inset))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat
        if collectionView == topArtistsCollectionView {
            height = 90
        } else {
            height = 70
        }
        return CGSize.init(width: UIScreen.main.bounds.width - 40, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectCell(in: collectionView, indexPath)
    }
    
    //MARK: NAvigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ArtistInfoViewController {
            let artistInfo = segue.destination as? ArtistInfoViewController
            artistInfo?.selectedArtist = selectedArtist
        }
    }
    
    
    
    //MARK: Private functions
    
    private func selectCell(in collectionView: UICollectionView, _ indexPath: IndexPath){
        if collectionView == topArtistsCollectionView {
            let selectedArtist = artists![indexPath.row]
            self.selectedArtist = selectedArtist
            let selectedCell = collectionView.cellForItem(at: indexPath)
            selectedCell?.contentView.backgroundColor = .white
            performSegue(withIdentifier: "artistInfo", sender: self)
        } else {
            print("tracks")
        }
    }
    
    private func loadTracks() {
        
        httpClientService.getTracks { (tracks, error) in
            if error != nil {
                print("ChartsViewControler -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                self.tracks = tracks
            }
            DispatchQueue.main.async {
                self.topSongsCollectionView.reloadData()
            }
        }

    }
    
    private func loadArtists() {
        httpClientService.getArtists { (artists, error) in
            if error != nil {
                print("ChartsViewControler -> loadArtists -> Error: \(error.debugDescription)")
            } else {
                self.artists = artists
            }
            DispatchQueue.main.async {
                self.topArtistsCollectionView.reloadData()
            }
        }
    }

}
