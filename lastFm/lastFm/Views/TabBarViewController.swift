//
//  TabBarViewController.swift
//  lastFm
//
//  Created by student on 3/27/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = UIColor.red
        setupTabBar()
        // Do any additional setup after loading the view.
    }
    
    func setupTabBar() {
        tabBar.barTintColor = UIColor(red: 252/255, green: 246/255, blue: 251/255, alpha: 1)
        tabBar.tintColor = .red
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
