//
//  TestTableViewCell.swift
//  test
//
//  Created by  Татьяна on 3/18/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class ArtistsTableViewCell: UITableViewCell {

    @IBOutlet weak var artistPhoto: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
