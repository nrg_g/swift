//
//  TracksTableViewController.swift
//  test
//
//  Created by student on 3/25/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class TopTracksTableViewController: UITableViewController {


    
    private let httpClient = URLSessionHTTPClient()
    
    var tracks: [Track]?
    
    var indexOfTheCurrentRow = 0
    
    var currentTrack: Track?
    
    var isLoading = false
    
    var pageOfTracks = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTracks()
    }

    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tracks?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "TopTracksTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TopTracksTableViewCell else {
            fatalError("TopTracksTableViewCell error")
        }
        let track = tracks![indexPath.row]
        cell.trackName.text = track.name
        
        if track.smallSizeImageUrl == "" {
            cell.trackImage.image = UIImage(named: "noImagePng")
        } else {
            if let url = URL(string: track.smallSizeImageUrl) {
                cell.trackImage.sd_setImage(with: url)
            }
        }
        return cell
    }
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let selectedArtist = tracks![indexPath.row]
//        currentTrack = selectedTrack
//        let selectedCell = tableView.cellForRow(at: indexPath)
//        selectedCell?.contentView.backgroundColor = tableView.backgroundColor
//        performSegue(withIdentifier: "showArtistInfo", sender: self)
//    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.destination is ArtistInfoViewController {
//            let artistInfo = segue.destination as? ArtistInfoViewController
//            artistInfo?.selectedArtist = currentArtist
//        }
//    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = tracks!.count - 1
        if (indexPath.row == lastElement) && !isLoading {
            loadTracks()
            isLoading = true
        }
    }
    
    private func loadTracks() {
        
        var tracks = [Track]()
        let parameters: [String: String]
        parameters = ["method": "chart.gettoptracks", "page": "\(pageOfTracks)", "api_key":  httpClient.apiKey, "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                NSLog("Error: \(error)")
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    tracks = try artistResponse.getTracksResponse()
                    
                    if(tracks.count > 50) {
                        self.tracks = tracks
                    } else {
                        if self.tracks != nil {
                            self.tracks! += tracks
                        } else {
                            self.tracks = []
                            self.tracks! += tracks
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.pageOfTracks += 1
                        self.isLoading = false
                    }
                } catch {
                    fatalError(error as! String)
                }
            }
        }
    }

}
