//
//  TopTracksTableViewCell.swift
//  test
//
//  Created by  Татьяна on 3/25/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class TopTracksTableViewCell: UITableViewCell {

    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
