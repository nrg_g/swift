//
//  TagSearchViewController.swift
//  test
//
//  Created by  Татьяна on 3/23/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class TagSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var tag: String = ""
    
    var artists: [Artist]?
    
    private let httpClient = URLSessionHTTPClient()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var searchResultTable: UITableView!
    
    var isLoading = false
    
    var pageOfArtists = 1
    
    var currentArtist: Artist!
    
    var isSearchButtonClicked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchResultTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        searchResultTable.dataSource = self
        searchResultTable.delegate = self
        searchBar.delegate = self
        searchBar.text = tag
        loadArtists()
    }
    
    //MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artists?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ArtistTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ArtistsTableViewCell else {
            fatalError("ArtistTableViewCell error")
        }
        
        let artist = artists![indexPath.row]
        cell.artistName.text = artist.name
        if artist.images?[2].url == "" {
            cell.artistPhoto.image = UIImage(named: "noImagePng")
        } else {
            if let url = URL(string: artist.images?[2].url ?? "") {
                cell.artistPhoto.sd_setImage(with: url)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastElement = artists!.count - 1
        if (indexPath.row == lastElement) && !isLoading {
            loadArtists()
            isLoading = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedArtist = artists![indexPath.row]
        currentArtist = selectedArtist
        let selectedCell = tableView.cellForRow(at: indexPath)
        selectedCell?.contentView.backgroundColor = tableView.backgroundColor
        performSegue(withIdentifier: "showArtistByTagInfo", sender: self)
    }
    
    //MARK: SearchBar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearchButtonClicked = true
        tag = searchBar.text ?? ""
        pageOfArtists = 1
        searchBar.resignFirstResponder()
        artists = []
        loadArtists()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    //MARK: Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ArtistInfoViewController {
            let artistInfo = segue.destination as? ArtistInfoViewController
            artistInfo?.selectedArtist = currentArtist
        }
    }
    
    //MARK: Private Functions
    
    private func scrollToTop() {
        let indexPath = NSIndexPath(row: 0, section: 0)
        searchResultTable.scrollToRow(at: indexPath as IndexPath, at: .top, animated: false)
    }
    
    private func loadArtists() {
        let parameters: [String: String]
        var artists = [Artist]()
        let tagName = tag.getTagName
        if tagName == "pop" && pageOfArtists == 1{
            pageOfArtists = 2
        }
        parameters = ["method": "tag.gettopartists", "tag": "\(tagName)", "page": "\(pageOfArtists)", "api_key":  httpClient.apiKey, "format": "json"]
        
        httpClient.get(parameters: parameters) { (artistResponse, error) in
            if let error = error {
                NSLog("Error: \(error)")
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    artists = try artistResponse.getArtistsByTagResponse()
                    
                    if(artists.count > 50) {
                        self.artists = artists
                    } else {
                        if self.artists != nil {
                            self.artists! += artists
                        } else {
                            self.artists = []
                            self.artists! += artists
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.searchResultTable.reloadData()
                        self.pageOfArtists += 1
                        self.isLoading = false
                        if self.isSearchButtonClicked {
                            self.scrollToTop()
                            self.isSearchButtonClicked = false
                        }
                    }
                } catch {
                    fatalError(error as! String)
                }
            }
        }
    }
    
    
}

extension String {
    var getTagName: String {
        if let firstIndex = self.firstIndex(of: "#") {
            return String(self.suffix(from: self.index(after: firstIndex)))
        }
        return self
    }
}

