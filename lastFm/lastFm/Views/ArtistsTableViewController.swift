//
//  TestTableViewController.swift
//  test
//
//  Created by  Татьяна on 3/18/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit
import SDWebImage
class ArtistsTableViewController: UITableViewController {

    private let httpClient = URLSessionHTTPClient()
    
    var artists: [Artist]?
    
    var indexOfTheCurrentRow = 0
    
    var currentArtist: Artist!
    
    var isLoading = false
    
    var pageOfArtists = 1
    
    override func viewDidLoad() {
        loadArtists()
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return artists?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ArtistTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ArtistsTableViewCell else {
            fatalError("ArtistTableViewCell error")
        }
        let artist = artists![indexPath.row]
        cell.artistName.text = artist.name
        
        if artist.images?[2].url == "" {
            cell.artistPhoto.image = UIImage(named: "noImagePng")
        } else {
            if let url = URL(string: artist.images?[2].url ?? "") {
                cell.artistPhoto.sd_setImage(with: url)
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedArtist = artists![indexPath.row]
        currentArtist = selectedArtist
        let selectedCell = tableView.cellForRow(at: indexPath)
        selectedCell?.contentView.backgroundColor = tableView.backgroundColor
        performSegue(withIdentifier: "showArtistInfo", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ArtistInfoViewController {
            let artistInfo = segue.destination as? ArtistInfoViewController
            artistInfo?.selectedArtist = currentArtist
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = artists!.count - 1
        if (indexPath.row == lastElement) && !isLoading {
            loadArtists()
            isLoading = true
            print("Hello")
        }
    }
    
    private func loadArtists() {
        
        var artists = [Artist]()
        let parameters: [String: String]
        parameters = ["method": "chart.gettopartists", "page": "\(pageOfArtists)", "api_key":  httpClient.apiKey, "format": "json"]
        
        httpClient.get(parameters: parameters) { artistResponse, error in
            if let error = error {
                NSLog("Error: \(error)")
            } else {
                do {
                    guard let artistResponse = artistResponse as? HttpResponse else {
                        fatalError("artistResponse error")
                    }
                    
                    artists = try artistResponse.getArtistsReponse()
                    
                    if(artists.count > 50) {
                        self.artists = artists
                    } else {
                        if self.artists != nil {
                            self.artists! += artists
                        } else {
                            self.artists = []
                            self.artists! += artists
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.pageOfArtists += 1
                        self.isLoading = false
                    }
                } catch {
                    fatalError(error as! String)
                }
            }
        }
    }
}

