//
//  ArtistsTracksSearchViewController.swift
//  lastFm
//
//  Created by student on 3/28/19.
//  Copyright © 2019 Gleb. All rights reserved.
//

import UIKit

class ArtistsTracksSearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    
    //MARK: Properties
    @IBOutlet weak var tracksCollectionView: UICollectionView!
    @IBOutlet weak var artistsCollectionView: UICollectionView!
    @IBOutlet weak var showArtistsButton: UIButton!
    @IBOutlet weak var showTracksButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let httpClientService = HTTPClientService()
    var buttonsAndCollections = [UIButton: UICollectionView]()
    var artists: [Artist]?
    var tracks: [Track]?
    let trackCollectionViewCellId = "SongCollectionViewCell"
    let artistCollectionViewCellId = "ArtistCollectionViewCell"
    var isLoading = false
    var pageOfArtists = 1
    var pageOfTracks = 1
    var searchString: String?
    var isSearchButtonClicked = false
    var currentButton: UIButton?
    var selectedArtist: Artist?
    var selectedColor = UIColor(red: 244/255, green: 155/255, blue: 154/255, alpha: 1)
    var defaultColor = UIColor(red: 252/255, green: 246/255, blue: 251/255, alpha: 1)

    override func viewDidLoad() {
        super.viewDidLoad()
        artistsCollectionView.delegate = self
        artistsCollectionView.dataSource = self
        tracksCollectionView.delegate = self
        tracksCollectionView.dataSource = self
        searchBar.delegate = self
        buttonsAndCollections = [showArtistsButton: artistsCollectionView, showTracksButton: tracksCollectionView]
        let nibCellSong = UINib(nibName: trackCollectionViewCellId, bundle: nil)
        let nibCellArtist = UINib(nibName: artistCollectionViewCellId, bundle: nil)
        artistsCollectionView.register(nibCellArtist, forCellWithReuseIdentifier: artistCollectionViewCellId)
        tracksCollectionView.register(nibCellSong, forCellWithReuseIdentifier: trackCollectionViewCellId)
        currentButton = showArtistsButton
        searchBar.placeholder = "LastFm music"
    }
    
    //MARK: Actions
    
    @IBAction func onArtistsButtonPressed(_ sender: UIButton) {
        changeTables(currentButton!, showArtistsButton)
    }
    @IBAction func onTracksButtonPressed(_ sender: UIButton) {
        changeTables(currentButton!, showTracksButton)
        
    }
    
    //MARK: CollectionView Functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !artistsCollectionView.isHidden {
            return artists?.count ?? 0
        } else {
            return tracks?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == artistsCollectionView {
            
            guard let cell = artistsCollectionView.dequeueReusableCell(withReuseIdentifier: artistCollectionViewCellId, for: indexPath) as? ArtistCollectionViewCell else {
                fatalError("ArtistCollectionViewCell error")
            }
            print(indexPath.row)
            let artist = artists![indexPath.row]
            cell.artistNameLabel.text = artist.name
            
            if artist.images?[2].url == "" {
                cell.artistImage.image = UIImage(named: "noImagePng")
            } else {
                if let url = URL(string: artist.images?[2].url ?? "") {
                    cell.artistImage.sd_setImage(with: url)
                }
            }
            cell.numberInChartLabel.text = "\(indexPath.row + 1)"
            cell.backgroundColor = .white
            return cell
            
        } else {
            
            guard let cell = tracksCollectionView.dequeueReusableCell(withReuseIdentifier: trackCollectionViewCellId, for: indexPath) as? SongCollectionViewCell else {
                fatalError("TrackTableViewCell error")
            }
            
            if let track = tracks?[indexPath.row] {
                cell.songNameLabel.text = track.name
            }
            let track = tracks?[indexPath.row]
            if track?.smallSizeImageUrl == "" {
                cell.songImage.image = UIImage(named: "noImagePng")
            } else {
                if let url = URL(string: (track?.smallSizeImageUrl)!) {
                    cell.songImage.sd_setImage(with: url)
                }
            }
            cell.artistNameLabel.text = track?.artistName
            cell.numberInChartLabel.text = "\(indexPath.row + 1)"
            cell.backgroundColor = .white
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = 5
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(inset))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == artistsCollectionView {
            let lastElement = artists!.count - 3
            if (indexPath.row == lastElement) && !isLoading {
                loadArtists()
                isLoading = true
            }
        } else {
            let lastElement = tracks!.count - 3
            if (indexPath.row == lastElement) && !isLoading {
                loadTracks()
                isLoading = true
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectCell(in: collectionView, indexPath)
    }
    
    //MARK: SearchBar
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let isSearchBarEmpty = searchBar.text?.isEmpty else {
            print("searchBar")
            return
        }
        if !isSearchBarEmpty {
            isSearchButtonClicked = true
            searchString = searchBar.text ?? ""
            pageOfArtists = 1
            pageOfTracks = 1
            searchBar.resignFirstResponder()
            artists = []
            loadArtists()
            tracks = []
            loadTracks()
            changeTables(showTracksButton, showArtistsButton)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        artists = []
        tracks = []
        print("text has been changed")
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ArtistInfoViewController {
            let artistInfo = segue.destination as? ArtistInfoViewController
            artistInfo?.selectedArtist = selectedArtist
        }
    }
    
    //MARK: Private Functions
    
    private func selectCell(in collectionView: UICollectionView, _ indexPath: IndexPath){
        if collectionView == artistsCollectionView {
            let selectedArtist = artists![indexPath.row]
            self.selectedArtist = selectedArtist
            let selectedCell = collectionView.cellForItem(at: indexPath)
            selectedCell?.contentView.backgroundColor = collectionView.backgroundColor
            performSegue(withIdentifier: "artistInfo", sender: self)
        } else {
            print("tracks")
        }
    }
    
    
    
    private func loadTracks() {
        let searchString = self.searchString ?? ""
        httpClientService.getListOfSongsBySearch(pageOfTracks: pageOfTracks, searchString: searchString) { (tracks, error) in
            if error != nil {
                print("TopSongsCollectionView -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                if tracks?.count ?? 0 > 50 {
                    self.tracks? += tracks!.suffix(from: tracks!.count - 50)
                }
                if self.tracks != nil {
                    self.tracks! += tracks ?? []
                } else {
                    self.tracks = []
                    self.tracks! += tracks ?? []
                }
            }
            DispatchQueue.main.async {
                self.tracksCollectionView.reloadData()
                self.pageOfTracks += 1
                self.isLoading = false
            }
        }
    }
    
    private func loadArtists() {
        let searchString = self.searchString ?? ""
        httpClientService.getListOfArtistsBySearch(pageOfArtists: pageOfArtists, searchString: searchString) { (artists, error) in
            if error != nil {
                print("TopArtistsCollectionView -> loadTracks -> Error: \(error.debugDescription)")
            } else {
                if artists?.count ?? 0 > 50 {
                    self.artists? += artists!.suffix(from: artists!.count - 50)
                } else {
                    if self.artists != nil {
                        self.artists! += artists ?? []
                    } else {
                        self.artists = []
                        self.artists! += artists ?? []
                    }

                }
                
                DispatchQueue.main.async {
                    self.artistsCollectionView.reloadData()
                    self.pageOfArtists += 1
                    self.isLoading = false
                }
            }
            
        }
        
        
    }
    
    private func changeTables(_ currentButton: UIButton, _ pressedButton: UIButton){
        guard let isSearchBarEmpty = searchBar.text?.isEmpty else {
            print("searchBar")
            return
        }
        if !isSearchBarEmpty {
            if currentButton == pressedButton {
                buttonsAndCollections[pressedButton]?.scrollToTop()
            } else {
                buttonsAndCollections[currentButton]?.isHidden = true
                buttonsAndCollections[pressedButton]?.isHidden = false
                buttonsAndCollections[pressedButton]?.reloadData()
                self.currentButton = pressedButton
                changeButtonsStyle(forPressedButton: pressedButton, forCurrentButton: currentButton)
            }
        }
    }
    
    private func changeButtonsStyle(forPressedButton pressedButton: UIButton,
                                    forCurrentButton currentButton: UIButton) {
        pressedButton.setTitleColor(.white, for: .normal)
        pressedButton.backgroundColor = selectedColor
        currentButton.setTitleColor(.red, for: .normal)
        currentButton.backgroundColor = defaultColor
        currentButton.isSelected = false
    }
    
}

